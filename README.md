**About MercerOS Platform - NLP**
MercerOS Platform - NLP APIs can be of good help If you're working with lot of text. It can help you with Natural Language Understanding and text analytics. For example, 
what's it about? 
What do the words mean in context? 
Who is doing what to whom? 
What companies and products are mentioned? 
Which texts are similar to each other?
etc..

MercerOS Platform - NLP consist of multiple APIs to extract information from text and perform vocabulary/ pattern training. The APIs as follows:


*** Create NLP model ***
This API helps in creating project specific language model. This helps in separating concerns by generating different language models for different projects. Projects can work independently without impacting other projects.

API URL : http://<server>/api/addnewmodel
Method Type : POST
Example Body : {
	        "modelname" : <mdoelname>
	    }
Output : {
            "success": "true",
            "message": [
                "Model initiated successfully\r"
            ]
        }
## server : This is the name or ip of the server where API is hosted
## modelname : This is the name of the model which has been created for the project

*** Get Parts of Speech ***
This API extracts multiple informtion from provided text and returns to user in JSON format. Information extract includes:

> Assignment word types to tokens, like verb or noun.
> Intent tree identification by assigning syntactic dependency labels, describing the relations between individual tokens, like subject or object
> Named entity recognition
> Text classification to specific categories (This is provided post category training)


API URL: http://<server>/api/getPOS/<modelname>/<user-query>
Method Type : Get
Output : {
	"success": "true",
	"message": {
		"Intents": [{}],
		"Nouns": [{}],
        "Verbs": [{}],
		"ADJ": [{}],
		"Parse_Tree": [{}	],
		"Entities": [{}],
		"POS": [{}]
	}
}
## user-query : This is the text or phrase around which pos information is required by user 
** Example **
<user-query> : "Apple is planning to buy oracle"
<Output> : 
{
	"success": "true",
	"message": {
		"Intents": [
			{
				"leavepolicy": 0.9999545812606812,
				"none": 0.00021214470325503498,
				"personality": 0.0005046966252848506,
				"sick": 0.00004539787187241018,
				"vacation": 0.2500241696834564
			}
		],
		"Nouns": [
			{
				"type": "PROPN",
				"value": "Apple"
			},
			{
				"type": "NOUN",
				"value": "oracle"
			}
		],
		"Verbs": [
			{
				"type": "VERB",
				"value": "is"
			},
			{
				"type": "VERB",
				"value": "planning"
			},
			{
				"type": "VERB",
				"value": "buy"
			}
		],
		"ADJ": [],
		"Parse_Tree": [
			{
				"node": "Apple",
				"parent": "planning"
			},
			{
				"node": "is",
				"parent": "planning"
			},
			{
				"node": "planning",
				"parent": "planning"
			},
			{
				"node": "to",
				"parent": "buy"
			},
			{
				"node": "buy",
				"parent": "planning"
			},
			{
				"node": "oracle",
				"parent": "buy"
			}
		],
		"Entities": [
			{
				"entity_type": "ORG",
				"value": "Apple"
			}
		],
		"POS": [
			{
				"word": "Apple",
				"pos": "PROPN_NNP"
			},
			{
				"word": "is",
				"pos": "VERB_VBZ"
			},
			{
				"word": "planning",
				"pos": "VERB_VBG"
			},
			{
				"word": "to",
				"pos": "PART_TO"
			},
			{
				"word": "buy",
				"pos": "VERB_VB"
			},
			{
				"word": "oracle",
				"pos": "NOUN_NN"
			}
		]
	}
}

*** Get Parse Tree ***

This API can help get parse tree information for provided sentences in tree format

API URL : http://<server>/api/getTree/:<modelname>/<user-query>
Method Type : GET

** Example ** 
<user-query> : "who is the tallest boy in the class?" 
<Output> : 
"         is              "
"  _______|_____           "
" |            boy        "
" |    _________|______    "
" |   |         |      in "
" |   |         |      |   "
" |   |         |    class"
" |   |         |      |   "
"who the     tallest  the "

*** Category Training ***

This API helps us assign a specific category name to similar types of texts.

API URL: http://<server>/api/traincategory/:<modelname>
Method Type : POST
Example Body : [ 
    ("How much vacation have I used?", "vacation"),
    ("Have I used any of my vacation?", "vacation"),
    ("Can I take a half day vacation?", "vacation"),
    ("Do I have to use vacation to volunteer?", "vacation"),
    ("Can I carry over vacation time?", "vacation"),
    ("Can I buy more vacation time?", "vacation"),
    ("How much time off do I have left?", "vacation")
]
Output : {
    "success": "true",
    "message": [
        "Training completed successfully"
    ]
}

*** Entity Training ***

This API help us map words used in sentences to named entities. Like in blow sentence Uber is used which can be mapped to Organization entity.

API URL: http://<server>/api/addentity/:<modelname>
Method Type : POST

Example Body : [
     ("Uber blew through $1 million a week", {'entities': [(0, 4, 'ORG')]}),
     ("Google rebrands its business apps", {'entities': [(0, 6, "ORG")]})
	]

Output : {
    "success": "true",
    "message": [
        "Training completed successfully"
    ]
}

*** Similar Sentences/ Utterances ***

This API can be used when you need to generate similar sentences (having synonyms) against an utterance

API URL: http://<server>/api/similarsentences/:<modelname>/:<utterance>
Method Type : GET

** Example **
<utterance> : "can i take leave for rehab?"
<output> :[
	'can i take leave for rehab program', 
	'can i take leave for psych ward', 
	'can i take leave for rehab facility', 
	'can i take leave for drug rehab', 
	'can i take leave for detox center', 
	'can i take leave for treatment center'
]

*** Conference Resolution ***

This API can be used find out words used in same context within a phrase/ text.

API URL: http://<server>/api/confresolution/:<phrase>
Method Type : GET

** Example **
<phrase> : "Barack Obama was born in Hawaii. He is the president. Obama was elected in 2008."
<output> :[
	['Barack Obama', 'He', 'the president', 'Obama'], 
	['Hawaii'], 
	['2008']
]