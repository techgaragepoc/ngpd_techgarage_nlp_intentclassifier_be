var http = require('http'),
    express = require('express'),
    bodyParser = require('body-parser'),
    config = require('./config'),
    lib = require('./lib'),
    PythonShell = require('python-shell');
session = require('express-session');
fs = require('fs');
Guid = require('guid');
request = require('request');

//const MongoStore = require('connect-mongo')(session);

var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.text());
// app.use(session({
//     store: new MongoStore({
//       url: config.session_store,
//       //url: 'mongodb://workdaybot:workdaybot@ds231360.mlab.com:31360/botframework_session',
//       autoRemove: 'interval',
//       autoRemoveInterval: 10 // In minutes. Default
//     }),
//     secret: 'somesecrettokenhere', 
//     resave: false,
//     saveUninitialized: true, 
//     cookie: { secure: false, maxAge: 600000 }// Secure is Recommeneded, However it requires an HTTPS enabled website (SSL Certificate)

// }))

//*** Allowing access to external request. Header initialization to enable api access from external applications
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header('Access-Control-Allow-Origin', '*');
    next();
});

//*** Defining api router
var intentclassifier = express.Router();
app.use('/api', intentclassifier);

//*** API Method track eventMaster
intentclassifier.route('/getPOS/:modelname')
    .post(function (req, res) {
        //*** Check if event data is provided in request body
        if (typeof req.body.userquery !== 'undefined') {
            var modelname = req.params.modelname;
            var userquery = req.body.userquery;
            //console.log(userquery);
            var options = {
                mode: 'text',
                pythonPath: config.python_path,
                scriptPath: './python/',
                args: [userquery, './python/' + modelname]
            };
            PythonShell.run('posprocessor.py', options, function (err, results) {
                if (err) throw err;
                res.status(200).json({ success: "true", message: JSON.parse(results) })
            });

        }
        else {
            res.status(500).json({ success: "false", message: "User query data missing in request" });
        }
    });


intentclassifier.route('/getPOS/:modelname/:userquery')
    .get(function (req, res) {
        //req.session.userquery += req.params.userquery;
        var modelname = req.params.modelname;
        var options = {
            mode: 'text',
            pythonPath: config.python_path,
            scriptPath: './python/',
            args: [req.params.userquery, './python/' + modelname]
        };
        PythonShell.run('posprocessor.py', options, function (err, results) {
            if (err) {
                throw err;
                res.status(500).json({ success: "false", message: err.message });
            }
            //console.log(results)
            res.status(200).json({ success: "true", message: JSON.parse(results) })
        });
        //console.log()
    });

intentclassifier.route('/getPOS/:username/:modelname/:userquery')
    .get(function (req, res) {
        //req.session.userquery += req.params.userquery;
        var modelname = req.params.modelname;
        var username = req.params.username;

        var options = {
            mode: 'text',
            pythonPath: config.python_path,
            scriptPath: './python/',
            //args: [decodeURIComponent(req.params.userquery), './python/nlp/' + username + '/' + modelname, modelname]
            args: [decodeURIComponent(req.params.userquery), './python/nlp/' + username + '/' + modelname, modelname]
        };
        PythonShell.run('posprocessor.py', options, function (err, results) {
            if (err) {
                throw err;
                res.status(500).json({ success: "false", message: err.message });
            }
            //console.log(results)
            res.status(200).json({ success: "true", message: JSON.parse(results) })
        });
        //console.log(req.params.userquery);
    });

intentclassifier.route('/getPOSByUser')
    .get(function (req, res) {
        var modelname = req.query.modelname;
        var username = req.query.username;
        var options = {
            mode: 'text',
            pythonPath: config.python_path,
            scriptPath: './python/',
            args: [decodeURIComponent(req.query.userquery), './python/nlp/' + username + '/' + modelname, 'xyz']
        };
        var pyFileName = 'posprocessor.py';
        //console.log(req.query.userquery,username,modelname);
        if (modelname == 'AIMEE(HR)'){
            pyFileName = 'posprocessor_aimee_min.py';
        }
        PythonShell.run(pyFileName, options, function (err, results) {
            if (err) {
                throw err;
                res.status(500).json({ success: "false", message: err.message });
            }
        res.status(200).json({ success: "true", message: JSON.parse(results) })
        //console.log("getPOSByUser Executed");
        });
    });

function deleteSpacyModel(path) {
    return new Promise(function (resolve, reject) {
        try {
            if (fs.existsSync(path)) {
                fs.readdirSync(path).forEach(function (file, index) {
                    var curPath = path + "/" + file;
                    if (fs.lstatSync(curPath).isDirectory()) {
                        deleteSpacyModel(curPath);
                    } else {
                        fs.unlinkSync(curPath);
                    }
                });
                fs.rmdirSync(path);
                resolve(true);
            }
            else {
                resolve(false);
            }
        }
        catch (err) {
            reject(err);
        }
    });
}
intentclassifier.route('/SpacyModel/:username/:modelname')
    .delete(function (req, res) {
        var modelname = req.params.modelname;
        var username = req.params.username;
        deleteSpacyModel('./python/nlp/' + username + '/' + modelname)
            .then(function (status) {
                if (status) {
                    res.status(200).json({ success: "true", message: 'Model deleted successfully.' });
                }
                else {
                    res.status(200).json({ success: "true", message: 'Model does not exist.' });
                }
            })
            .catch(function (err) {
                res.status(500).json({ success: "false", message: err.message });
            });
    });
intentclassifier.route('/SpacyModel/:modelname')
    .delete(function (req, res) {
        var modelname = req.params.modelname;
        deleteSpacyModel('./python/nlp/' + modelname)
            .then(function (status) {
                if (status) {
                    res.status(200).json({ success: "true", message: 'Model deleted successfully.' });
                }
                else {
                    res.status(200).json({ success: "true", message: 'Model does not exist.' });
                }
            })
            .catch(function (err) {
                res.status(500).json({ success: "false", message: err.message });
            });
    });
intentclassifier.route('/getNames/:userquery')
    .get(function (req, res) {
        var options = {
            mode: 'text',
            pythonPath: config.python_path,
            scriptPath: './python/',
            args: [req.params.userquery]
        };
        PythonShell.run('NamedEntityRecognition.py', options, function (err, results) {
            if (err) {
                throw err;
                res.status(500).json({ success: "false", message: err.message });
            }
            res.status(200).json({ success: "true", name: results })
        });
    });



intentclassifier.route('/similarsentences/:modelname/:utterance')
    .get(function (req, res) {
        var modelname = req.params.modelname
        var options = {
            mode: 'text',
            pythonPath: config.python_path,
            scriptPath: './python/',
            args: [req.params.utterance, './python/' + modelname]
        };
        PythonShell.run('synonymgenerator.py', options, function (err, results) {
            if (err) {
                throw err;
                res.status(500).json({ success: "false", message: err.message });
            }
            //console.log(results)
            res.status(200).json({ success: "true", message: results })
        });
    });

intentclassifier.route('/confresolution/:phrase')
    .get(function (req, res) {
        var modelname = req.params.modelname
        var options = {
            mode: 'text',
            pythonPath: config.python_path,
            scriptPath: './python/',
            args: [req.params.phrase]
        };
        PythonShell.run('confresolver.py', options, function (err, results) {
            if (err) {
                throw err;
                res.status(500).json({ success: "false", message: err.message });
            }
            //console.log(results)
            res.status(200).json({ success: "true", message: results })
        });
    });

intentclassifier.route('/getTree/:modelname/:userquery')
    .get(function (req, res) {
        var modelname = req.params.modelname;
        var options = {
            mode: 'text',
            pythonPath: config.python_path,
            scriptPath: './python/',
            pythonOptions: ['-u'],
            args: [req.params.userquery, './python/' + modelname]
        };
        // console.log(options);
        PythonShell.run('printspacytree.py', options, function (err, results) {
            if (err) {
                // res.status(500).json({"error": err});
            }
            res.status(200).send(results);
        });
    });



intentclassifier.route('/getTreeByUser')
    .get(function (req, res) {
        var modelname = req.query.modelname;
        var username = req.query.username;
        var options = {
            mode: 'text',
            pythonPath: config.python_path,
            scriptPath: './python/',
            pythonOptions: ['-u'],
            args: [req.query.userquery, './python/nlp/' + username + '/' + modelname, 'abcd']

        };
        // console.log(options);
        PythonShell.run('printspacytree.py', options, function (err, results) {
            if (err) {
                // res.status(500).json({"error": err});
                throw err;
            }
            res.status(200).send(results);
        });
    });


intentclassifier.route('/addentity/:modelname')
    .post(function (req, res) {
        //*** Check if event data is provided in request body
        if (typeof req.body !== 'undefined') {
            var entitydata = req.body;
            var modelname = req.params.modelname;
            console.log(entitydata);
            var options = {
                mode: 'text',
                pythonPath: config.python_path,
                scriptPath: './python/',
                args: [entitydata, './python/' + modelname]
            };
            var process =
                PythonShell.run('trainspacyentity.py', options, function (err, results) {
                    //                if (err) throw err;
                    console.log(results)
                    if (results == "Training completed successfully\r") {
                        res.status(200).json({ success: "true", message: results })
                    }
                    else {
                        res.status(500).json({ success: "false", message: null, error: err })
                    }

                });

        }
        else {
            res.status(500).json({ success: "false", message: "User query data missing in request" });
        }
    });

intentclassifier.route('/traindependency/:modelname')
    .post(function (req, res) {
        //*** Check if event data is provided in request body
        if (typeof req.body !== 'undefined') {
            var modelname = req.params.modelname;
            var dependencydata = req.body;
            console.log(dependencydata);
            var options = {
                mode: 'text',
                pythonPath: config.python_path,
                scriptPath: './python/',
                args: [dependencydata, './python/' + modelname]
            };
            PythonShell.run('updatedependencyparser.py', options, function (err, results) {
                if (err) throw err;
                console.log(results)
                if (results == "Training completed successfully\r") {
                    res.status(200).json({ success: "true", message: results })
                }
                else {
                    res.status(500).json({ success: "false", message: results })
                }

            });

        }
        else {
            res.status(500).json({ success: "false", message: "Dependency data missing in request" });
        }
    });

intentclassifier.route('/trainpostagger/:modelname')
    .post(function (req, res) {
        //*** Check if event data is provided in request body
        if (typeof req.body !== 'undefined') {
            var modelname = req.params.modelname;
            var train_data = req.body;
            var options = {
                mode: 'text',
                pythonPath: config.python_path,
                scriptPath: './python/',
                args: [train_data, './python/' + modelname]
            };
            PythonShell.run('updatepostagger.py', options, function (err, results) {
                if (err) throw err;
                if (results == "Training completed successfully\r") {
                    res.status(200).json({ success: "true", message: results })
                }
                else {
                    res.status(500).json({ success: "false", message: results })
                }

            });

        }
        else {
            res.status(500).json({ success: "false", message: "Training data missing in request" });
        }
    });
intentclassifier.route('/addnewmodel')
    .post(function (req, res) {
        //*** Check if event data is provided in request body
        if (typeof req.body !== 'undefined') {
            var modelname = req.body.modelname;
            console.log(modelname);
            var options = {
                mode: 'text',
                pythonPath: config.python_path,
                scriptPath: './python/',
                args: ['./python/' + modelname]
            };
            PythonShell.run('addnewmodel.py', options, function (err, results) {
                if (err) throw err;
                console.log(results);
                if (results == "Model initiated successfully\r") {
                    res.status(200).json({ success: "true", message: results });
                }
                else {
                    res.status(500).json({ success: "false", message: results });
                }

            });

        }
        else {
            res.status(500).json({ success: "false", message: "Dependency data missing in request" });
        }
    });

intentclassifier.route('/traincategory/:modelname')
    .post(function (req, res) {
        //*** Check if event data is provided in request body
        if (typeof req.body !== 'undefined') {
            var modelname = req.params.modelname;
            var intentdata = req.body;
            console.log(intentdata);
            var options = {
                mode: 'text',
                pythonPath: config.python_path,
                scriptPath: './python/',
                args: [intentdata, './python/' + modelname]
            };
            PythonShell.run('text_classification.py', options, function (err, results) {
                if (err) throw err;
                console.log(results)
                if (results == "Training completed successfully\r") {
                    res.status(200).json({ success: "true", message: results })
                }
                else {
                    res.status(500).json({ success: "false", message: results })
                }

            });

        }
        else {
            res.status(500).json({ success: "false", message: "Intent data missing in request" });
        }
    });

intentclassifier.route('/trainspacymodel')
    .post(function (req, res) {
        //*** Check if event data is provided in request body
        const fileName = './python/json/' + Guid.create().value + '.json';
        if (typeof req.body.model !== 'undefined' && typeof req.body.user !== 'undefined') {
            var model = req.body.model;
            var user = req.body.user;
            var mName = req.body.model.modelName;
            var cbURL = req.body.model.callbackUrl;
            model.modelName = user + '/' + model.modelName;
            var input = JSON.stringify(model);
            fs.writeFileSync(fileName, input, 'utf-8');
            var options = {
                mode: 'text',
                pythonPath: config.python_path,
                scriptPath: './python/',
                pythonOptions: ['-u'],
                args: [fileName]
            };
            console.log('Training Initiated for :', model.modelName)
            var start = Date.now();
            console.log("Training in progress...");
            PythonShell.run('TrainDomainModel.py', options, function (err, results) {
                setTimeout(function () {
                    var millis = Date.now() - start;
                    console.log("Training completed & seconds elapsed are = " + Math.floor(millis / 1000));
                }, 2000);
                console.log('Spacy task completed with response status code :', res.statusCode)
                if (typeof results !== 'undefined' && results.length > 0) {
                    //res.status(200).json({ success: "true", message:"Training completed successfully",utterances:results[1]});
                    console.log("Preparing call back response...")
                    cbpayload = { "modelName": mName, "utterances": JSON.parse(results[0])}
                    var cboptions = {
                        method: 'PUT',
                        url: cbURL,
                        json: cbpayload,
                        headers:
                        {
                            'Cache-Control': 'no-cache',
                            'Content-Type': 'application/json'
                        }
                    }
                    //console.log(cbpayload)
                    request(cboptions, function (error, response, body) {
                        //console.log("Response received : ", response)
                        if (err)
                            res.status(500).json({ success: "false", message: null, error: err.traceback });
                        if (response) {
                        console.log(response.statusCode,response.body.success)
                            switch (response.statusCode) {
                                // case (500): {res.status(500).json({ success: false})};
                                case (200): { res.status(200).json({ success: true,utterances:results[0]}); break; }
                                default: { res.status(500).json({ success: false }); break; }
                            }
                        }
                    });
                    fs.unlinkSync(fileName);
                }
                else {
                    res.status(500).json({ success: "false", message: "No results generated from training. Please compile traindomainmodel.py" });
                }
            });
        }
        else {
            res.status(500).json({ success: "false", message: "Dependency data missing in request" });
        }
    });

//*** Starting server 
var port = process.env.PORT || config.port;
try {
    app.listen(port, function () {
        console.log('listening in http://' + config.hostname + ':' + port);
    });
}
catch (err) {
    console.log(err.message);
}
module.exports = app;