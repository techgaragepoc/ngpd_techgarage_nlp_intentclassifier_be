var config = module.exports = {};

config.env = 'development';
config.hostname = 'localhost';
config.port = '5003';
config.python_path = 'C:/Users/prautela_app/AppData/Local/Programs/Python/Python36-32/python';
config.session_store = 'mongodb://localhost/botframework_session';

// Environment values
config.environment = {}
config.environment.development = 'development';
config.environment.test = 'test';
config.environment.production = 'production';


// mongo database
config.mongo = {}
config.mongo.uri = 'mongodb://localhost/eventtrackerdb';


