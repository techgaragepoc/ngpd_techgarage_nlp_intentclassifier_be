var env = process.env.NODE_ENV || 'production',
    config = require('./config.' + env);

config.mongo.uri = process.env.MONGO_URI || config.mongo.uri;

module.exports = config;