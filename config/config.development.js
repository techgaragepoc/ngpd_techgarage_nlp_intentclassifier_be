var config = require('./config.global');

config.env = 'development';
config.logger.level = 'DEBUG';


module.exports = config;