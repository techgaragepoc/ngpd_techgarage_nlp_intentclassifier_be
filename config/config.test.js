var config = require('./config.global');

config.env = 'test';
config.logger.level = 'ERROR';

module.exports = config;