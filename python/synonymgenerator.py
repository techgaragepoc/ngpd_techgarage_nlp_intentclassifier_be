import spacy
from sense2vec import Sense2VecComponent
import sys

model = sys.argv[2]
nlp = spacy.load(model)
s2v = Sense2VecComponent('./reddit_vectors-1.1.0')
nlp.add_pipe(s2v)

sentence = sys.argv[1]
doc = nlp(u'' + sentence)
sampleSentences = []
for chunk in doc.noun_chunks:
    _doc = nlp(u'' + chunk.text)
    if _doc[0]._.in_s2v:
        most_similar = _doc[0]._.s2v_most_similar(10)
        for synonym, score in most_similar:
            if (chunk.text.lower() != synonym[0].lower()) and (score > 0.80):
                _sentence = sentence.replace(chunk.text, synonym[0])
                sampleSentences.append(_sentence)
print(sampleSentences)
