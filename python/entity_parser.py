import json
from collections import Counter
import pandas as pd
import os.path

def loadSynonyms(filepath):
    #file_path = os.path.abspath(os.path.dirname(__file__))
    #file_path = os.path.dirname(__file__)
    training_file = os.path.join(filepath,'./synonyms.json')
    #training_file = './synonyms.json'

    data = json.load(open(training_file))
    dataframe = pd.DataFrame(data)
    return dataframe.rasa_nlu_data.entity_synonyms

stopwords = [
    'what',
    'where',
    'how',
    'is',
    'are',
    'were',
    'was',
    'has',
    'had',
    'have',
    '!',
    '?',
    '.'
]

def counterSubset(list1, list2):
    c1, c2 = Counter(list1), Counter(list2)
    for k, n in c1.items():
        if n > c2[k]:
            return False
    return True
def removeStopWords(userquery):
    text = ' '.join([word for word in userquery.split() if word not in (stopwords)])
    text = text.replace('?','').replace('!', '').replace('.', '')
    return text
def getentity(filepath, userquery):
    entity_synonyms = loadSynonyms(filepath)
    userquery  = removeStopWords(userquery)
    expected_output = []
    for data_array in entity_synonyms:
        for element in data_array['synonyms']:
            if type(element) != dict:
                querytext = userquery.lower()
                element = element.lower()
            else:
                if(element['casematch']== 'true'):
                    element = element['value']
                    querytext = userquery
                else:
                    element = element['value'].lower()
                    querytext = userquery.lower()
            if querytext.find(element) != -1 and counterSubset(element.split(), userquery.split()):
                expected_output.append({ 'Entity_Type' : data_array['value'],'value':element})
                break
    return expected_output
#print(getentity('is it your name'))
#print(removeStopWords('i want my colleague to create a career story. where is the template'))
