import json
from collections import Counter
import pandas as pd
import os.path


def loadIntentFile(filepath):
    #file_path = os.path.abspath(os.path.dirname(__file__))
    intent_file = os.path.join(filepath, './static_intents.json')
    #intent_file = './static_intents.json'
    return json.load(open(intent_file))

def getstaticintent(filepath, entity_list):
    intent_data = loadIntentFile(filepath)
    output = ''
#    for entity in entity_list:
    for Entity_Type in entity_list:
        for item in intent_data:
 #           if item['entity'] == entity['entity']:
            if item['entity'] == Entity_Type['Entity_Type']:
                output = item['intent']
    return output