import spacy 
import json
import sys
import entity_parser
import get_static_intent
import numpy 


def getNLPresponse(userquery,username,modelname):
    model_path = './python/nlp/' + username + '/' + modelname
    nlp = spacy.load(model_path)
    custom_entities = entity_parser.getentity(model_path, userquery)
    intent =  get_static_intent.getstaticintent(model_path, custom_entities)
    if intent == '':
        doc = nlp(u'' + userquery) 
        intent = max(doc.cats, key=doc.cats.get)
    output = {
        'modelname':model_path,
        'Intents':intent,
        'Nouns':[],
        'Verbs':[],
        'ADJ':[],
        'Parse_Tree':[],
        'Entities': custom_entities,
        'POS':[]
    }
    # output = {
    # 'intent': intent,
    # 'entity_synonyms': custom_entities
    # }
    return (json.dumps(output))