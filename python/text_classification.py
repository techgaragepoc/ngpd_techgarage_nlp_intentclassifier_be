import random
from pathlib import Path
import spacy
import numpy as np
from spacy.util import minibatch, compounding
import sys

input_Data = eval(sys.argv[1])
modelname = sys.argv[2]
output_dir = modelname
n_iter = 20

def dictionerize_cats(y_train):
    l = []
    all_labels = np.unique(y_train) 
    for curr_label in y_train:
        l.append({cat: bool(cat==curr_label) for cat in all_labels})
    return l

X_train, Y_train = zip(*input_Data)
dic_cats = dictionerize_cats(Y_train)
train_data = list(zip(X_train, [{'cats': cats} for cats in dic_cats]))


def train_model(model=None, output_dir=None, n_iter=20):
    if model is not None:
        nlp = spacy.load(model)  # load existing spaCy model
    else:
        nlp = spacy.blank('en')  # create blank Language class

    #other_pipes = [pipe for pipe in nlp.pipe_names]
    if(len(nlp(u'Test').cats) > 0):
        nlp.disable_pipes('textcat')
    
    # add the text classifier to the pipeline if it doesn't exist
    # nlp.create_pipe works for built-ins that are registered with spaCy
    #if 'textcat' not in nlp.pipe_names:
    if 'textcat' not in nlp.pipe_names:
        textcat = nlp.create_pipe('textcat')
        nlp.add_pipe(textcat, last=True)
    # otherwise, get it, so we can add labels to it
    else:
        textcat = nlp.get_pipe('textcat')

    # add label to text classifier
    for cat in np.unique(Y_train):
        if not cat in textcat.labels:
            textcat.add_label(cat)

    # get names of other pipes to disable them during training
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != 'textcat']
    with nlp.disable_pipes(*other_pipes):  # only train textcat
        optimizer = nlp.begin_training()
        for i in range(n_iter):
            losses = {}
            # batch up the examples using spaCy's minibatch
            batches = minibatch(train_data, size=compounding(4., 32., 1.001))
            for batch in batches:
                texts, annotations = zip(*batch)
                nlp.update(texts, annotations, sgd=optimizer, drop=0.2,
                           losses=losses)

    if output_dir is not None:
        output_dir = Path(output_dir)
        if not output_dir.exists():
            output_dir.mkdir()
        nlp.to_disk(output_dir)
    print("Training completed successfully")

def evaluate(tokenizer, textcat, texts, cats):
    docs = (tokenizer(text) for text in texts)
    tp = 1e-8  # True positives
    fp = 1e-8  # False positives
    fn = 1e-8  # False negatives
    tn = 1e-8  # True negatives
    for i, doc in enumerate(textcat.pipe(docs)):
        gold = cats[i]
        for label, score in doc.cats.items():
            if label not in gold:
                continue
            if score >= 0.5 and gold[label] >= 0.5:
                tp += 1.
            elif score >= 0.5 and gold[label] < 0.5:
                fp += 1.
            elif score < 0.5 and gold[label] < 0.5:
                tn += 1
            elif score < 0.5 and gold[label] >= 0.5:
                fn += 1
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    f_score = 2 * (precision * recall) / (precision + recall)
    return {'textcat_p': precision, 'textcat_r': recall, 'textcat_f': f_score}


train_model(output_dir, output_dir, n_iter)



