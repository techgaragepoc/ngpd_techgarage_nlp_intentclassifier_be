#import json
#from stanfordcorenlp import StanfordCoreNLP

#nlp = StanfordCoreNLP('../stanford-corenlp-full-2018-02-27', quiet=False)
#props = {'annotators': 'coref', 'pipelineLanguage': 'en', "ner.useSUTime" : "false"}
#props.setProperty("ner.useSUTime", "false")

#text = 'Barack Obama was born in Hawaii.  He is the president. Obama was elected in 2008.'
#result = json.loads(nlp.annotate(text, properties=props))

#corefs = []
#for k, mentions in result['corefs'].items():
#    simplified_mentions = []
#    for m in mentions:
#        simplified_mentions.append((m['sentNum'], m['startIndex'], m['endIndex'], m['text']))
#    corefs.append(simplified_mentions)
#print(corefs)

from stanfordcorenlp import StanfordCoreNLP
import logging
import json
import sys

phrase = sys.argv[1]


class StanfordNLP:
    def __init__(self, host='http://localhost', port=9001):
        self.nlp = StanfordCoreNLP(host, port=port,
                                   timeout=30000)  # , quiet=False, logging_level=logging.DEBUG)
        self.props = {
            'annotators': 'tokenize,ssplit,pos,lemma,ner,parse,depparse,dcoref,relation',
            'pipelineLanguage': 'en',
            'outputFormat': 'json',
            "ner.useSUTime" : "false"
        }

    def word_tokenize(self, sentence):
        return self.nlp.word_tokenize(sentence)

    def pos(self, sentence):
        return self.nlp.pos_tag(sentence)

    def ner(self, sentence):
        return self.nlp.ner(sentence)

    def parse(self, sentence):
        return self.nlp.parse(sentence)

    def dependency_parse(self, sentence):
        return self.nlp.dependency_parse(sentence)

    def annotate(self, sentence):
        return json.loads(self.nlp.annotate(sentence, properties=self.props))

    @staticmethod
    def tokens_to_dict(_tokens):
        tokens = defaultdict(dict)
        for token in _tokens:
            tokens[int(token['index'])] = {
                'word': token['word'],
                'lemma': token['lemma'],
                'pos': token['pos'],
                'ner': token['ner']
            }
        return tokens


sNLP = StanfordNLP()
#text = "I have purchased a new policy. Can you please tell me it's value"
#text = "Barack Obama was born in Hawaii.  He is the president. Obama was elected in 2008."

result =  sNLP.annotate(phrase)
corefs = []
for k, mentions in result['corefs'].items():
    simplified_mentions = []
    #mention = ""
    for m in mentions:
        #print(m)
        #if(m['isRepresentativeMention']):
        #    mention = m['text']
        #    simplified_mentions.append((m['text'], ''))
        #else:
        simplified_mentions.append((m['text']))
    corefs.append(simplified_mentions)
print(corefs)

#print(corefs)
#print "POS:", sNLP.pos(text)
#print "Tokens:", sNLP.word_tokenize(text)
#print "NER:", sNLP.ner(text)
#print "Parse:", sNLP.parse(text)
#print "Dep Parse:", sNLP.dependency_parse(text)