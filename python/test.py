import random
from pathlib import Path
import spacy
import numpy as np
from spacy.util import minibatch, compounding
import sys

input_Data = [ 
(u'How many sick days do we get', u'sick')
]
modelname = "./workdaybot"
output_dir = modelname
n_iter = 20

def dictionerize_cats(y_train):
    l = []
    all_labels = np.unique(y_train) 
    for curr_label in y_train:
        l.append({cat: bool(cat==curr_label) for cat in all_labels})
    return l

X_train, Y_train = zip(*input_Data)
dic_cats = dictionerize_cats(Y_train)
train_data = list(zip(X_train, [{'cats': cats} for cats in dic_cats]))


def train_model(model=None, output_dir=None, n_iter=20):
    if model is not None:
        nlp = spacy.load(model)  # load existing spaCy model
    else:
        nlp = spacy.blank('en')  # create blank Language class

    #other_pipes = [pipe for pipe in nlp.pipe_names]
    #nlp.disable_pipes(*other_pipes)

    # add the text classifier to the pipeline if it doesn't exist
    # nlp.create_pipe works for built-ins that are registered with spaCy
    if 'textcat' not in nlp.pipe_names:
        textcat = nlp.create_pipe('textcat')
        nlp.add_pipe(textcat, last=True)
    # otherwise, get it, so we can add labels to it
    else:
        textcat = nlp.get_pipe('textcat')

    print(textcat.labels)
    # add label to text classifier
    for cat in np.unique(Y_train):
        print(cat)
        #if not cat in textcat.labels:
        textcat.add_label('sick')

print("Hello".lower())
#train_model(output_dir, output_dir, n_iter)