import random
from pathlib import Path
import spacy
import sys

output_dir = sys.argv[1]

def addnewmodel(output_dir=None):
    """Load the model, set up the pipeline and train the parser."""
    nlp = spacy.load('en')  # load existing spaCy model
    if output_dir is not None:
        output_dir = Path(output_dir)
        if not output_dir.exists():
            output_dir.mkdir()
        nlp.to_disk(output_dir)
        print("Model initiated successfully")
    else:
        print("Please provide model name")

addnewmodel(output_dir)