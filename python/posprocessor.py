import spacy 
import json
import sys

from spacy.matcher import PhraseMatcher
from spacy.tokens import Span

class EntityMatcher(object):
    name = ""

    def __init__(self, nlp, terms, label):
        self.label = nlp.vocab.strings[label]  
        self.name = self.label
        patterns = [nlp(text) for text in terms]
        self.matcher = PhraseMatcher(nlp.vocab)
        self.matcher.add(label, None, *patterns)

    def __call__(self, doc):
        matches = self.matcher(doc)
        for match_id, start, end in matches:
            span = Span(doc, start, end, label=self.label)
            doc.ents = list(doc.ents) + [span]
        return doc

model = sys.argv[2]
modelName = sys.argv[3]
#modelName = 'en'
#if modelName == 'xyz':
#    nlp = spacy.load('en')
#else:
#    nlp = spacy.load(model)

if modelName.upper() == 'EN':
	nlp = spacy.load('en')
else:
	nlp = spacy.load(model)	

#terms = ['pm']
#nlp.entity.add_label('ambiguity')
#entity_matcher = EntityMatcher(nlp, terms, 'ambiguity')
#nlp.add_pipe(entity_matcher, last=True)#ambiguity

#terms = ['project manager','project managers', 'people manager', 'people managers', 'Project Manager', 'People Manager']
#nlp.entity.add_label('role')
#entity_matcher = EntityMatcher(nlp, terms, 'role')
#nlp.add_pipe(entity_matcher, last=True)#role

#terms = ['yesterday', 'day before yesterday', 'today', 'tomorrow','this week', 'current week','last week', 'this month', 'current #month','last month', 'this year','current month', 'last year', 'this quarter', 'current quarter','previous quarter', 'current financial #year', 'this financial year']
#nlp.entity.add_label('timeseriesfx')
#entity_matcher = EntityMatcher(nlp, terms, 'timeseriesfx')
#nlp.add_pipe(entity_matcher, last=True)#time series functions

#terms = ['total', 'avg', 'average', 'min','mean', 'minimum', 'max', 'maximum', 'variance', 'difference']
#nlp.entity.add_label('Aggfx')
#entity_matcher = EntityMatcher(nlp, terms, 'Aggfx')
#nlp.add_pipe(entity_matcher, last=True)#fx


#terms = ['less than','less than equal to', 'greater than', 'greater than equal to', 'exactly equal to','not equal to']
#nlp.entity.add_label('operators')
#entity_matcher = EntityMatcher(nlp, terms, 'operators')
#nlp.add_pipe(entity_matcher, last=True)#operators


#terms = ['$','>','>=','=','<','<=']
#nlp.entity.add_label('symbol')
#entity_matcher = EntityMatcher(nlp, terms, 'symbol')
#nlp.add_pipe(entity_matcher, last=True)#operators

#terms = ['sunday','monday','tuesday','wednesday','thursday','friday','saturday']
#nlp.entity.add_label('weekday')
#entity_matcher = EntityMatcher(nlp, terms, 'weekday')
#nlp.add_pipe(entity_matcher, last=True)#weekdays

#terms = #['january','January','february','February','march','March','april','April','May','june','June','july','July','august','August','september',#'September','october','October','november','November','december','December'] #except may
#nlp.entity.add_label('monthname')
#entity_matcher = EntityMatcher(nlp, terms, 'monthname')
#nlp.add_pipe(entity_matcher, last=True)#monthnames

#terms = ['wealth', 'talent', 'career','human resources','health']
#nlp.entity.add_label('domain')
#entity_matcher = EntityMatcher(nlp, terms, 'domain')
#nlp.add_pipe(entity_matcher, last=True)#domain values

#terms = ['europac', 'north america', 'north-america','apac','APAC','EUROPAC','NORTH AMERICA']
#nlp.entity.add_label('region')
#entity_matcher = EntityMatcher(nlp, terms, 'region')
#nlp.add_pipe(entity_matcher, last=True)#custom region names

#terms = ['premium','claims','brokerage','accounts receivable','ar','cyr','revenue','renumeration']
#nlp.entity.add_label('metrics')
#entity_matcher = EntityMatcher(nlp, terms, 'metrics')
#nlp.add_pipe(entity_matcher, last=True)#metrics

doc = nlp(u' '+ sys.argv[1]) 

counter = 0
#data =  '{ "modelName": "' +modelName +'", "Intents" : [{"' + 'NA' + '" : "' + 'NA' + '"}'
data =  '{ "modelName": "' +model +'", "Intents" : [' + str(doc.cats).replace("'", '"')
data += '], "Nouns" : ['
for token in doc:
    data +=  ''
    if (token.pos_ == "PROPN" or token.pos_ == "NOUN" or token.pos_ == "NOUN_N" ):
        if(counter > 0):
            data += ','
        data += '{ "type" : "' + token.pos_ + '", "value" : "' + token.text + '", "lemma" : "' + token.lemma_ + '"}' 
        counter = counter + 1
data += '], "Verbs" : ['
counter= 0
for token in doc:
    if (token.pos_ == "VERB" or token.pos_ == "VERB_VBN"):
        if(counter > 0):
            data += ','
        data += '{ "type" : "' + token.pos_ + '", "value" : "' + token.text + '", "lemma" : "' + token.lemma_ + '"}' 
        counter = counter + 1
data += '], "ADJ" : ['
counter = 0
for token in doc:
    if (token.pos_ == "ADJ" and token.is_stop == False ):
        if(counter > 0):
            data += ','
        data += '{ "type" : "' + token.pos_ + '", "value" : "' + token.text + '", "lemma" : "' + token.lemma_ + '"}' 
        counter = counter + 1
    #print(token.text, token.lemma_, token.pos_, token.tag_, token.dep_,
    #      token.shape_, token.is_alpha, token.is_stop)
data += ' ], "Parse_Tree" : ['
counter = 0

for token in doc:
    if(counter > 0):
        data += ','
    data += '{ "node" : "' + token.text + '", "parent" : "' + token.head.text + '"}' #, "child" : "' + ''.join([str(child) for child in token.children]) + '"},' 
    counter = counter + 1
data += ' ], "Entities" : ['
counter = 0

for ent in doc.ents:
    if(counter > 0):
        data += ','
    data += '{ "entity_type" : "' + ent.label_ + '", "value" : "' + ent.text + '"}' 
    counter = counter + 1
data += '], "POS" : ['
counter = 0

for token in doc:
    if(counter > 0):
        data += ','
    data += '{ "word" : "' + token.text + '", "pos" : "' + token.pos_
    if(token.tag_ != '""'):
        data += '_' + token.tag_
    data += '"}'
    counter = counter + 1
data += ']'
data += '}'
json_Data = json.dumps(data)

print(json_Data)
