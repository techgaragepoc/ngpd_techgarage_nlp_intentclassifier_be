from pathlib import Path
import random
import spacy
import json
import numpy as np
from spacy.util import minibatch, compounding
import sys
import os

ModelName = ''
Version = ''
Description = ''
Language = ''
Category = ''
Entity = ''

#C:\Spacy\python\json\
#with open('./json/305a2239-a497-bca1-283b-77e6ad969a16.json') as json_file:  
with open(sys.argv[1]) as json_file:  
    data = json.load(json_file)
    ModelName = './python/nlp/' + data['modelName']
    Version = data['versionId']
    Description = data['desc']
    Language = data['culture']
    Category = '['
    Entity = '['
    for utterance in data['utterances']:
        Category += "('" + utterance['text'] +"','" + utterance['category'] + "'),"
        entityString = "{'entities': ["
        for entity in utterance['entities']:
            entityString += "(" + str(entity['startIndex']) + "," + str(entity['endIndex']) + ",'" + entity['name'] + "'),"
        if(len(entityString) > 14):
            entityString = entityString[:-1]
        entityString += ']}'
        Entity += "('" + utterance['text'] + "'," + entityString + "),"
    if(len(Category) > 1):
        Category = Category[:-1]
    if(len(Entity) > 1):
        Entity = Entity[:-1]
    Category += ']'
    Entity += ']'

output_dir = ModelName
input_Data = eval(Category)
TRAIN_DATA = eval(Entity)
n_iter = 2

# creating folder with User Name to store NLP model
if not os.path.exists(ModelName):
    os.makedirs(ModelName)

def dictionerize_cats(y_train):
    l = []
    all_labels = np.unique(y_train) 
    for curr_label in y_train:
        l.append({cat: bool(cat==curr_label) for cat in all_labels})
    return l

X_train, Y_train = zip(*input_Data)
dic_cats = dictionerize_cats(Y_train)
train_data = list(zip(X_train, [{'cats': cats} for cats in dic_cats]))

if ModelName.upper() == 'EN':
    nlp = spacy.load('en')
else:
    nlp = spacy.load(ModelName)
#nlp = spacy.load('en')
#print("Starting with pipes: ",nlp.pipe_names)
# def train_entity(model=None, output_dir=None, n_iter=20):
#     if 'ner' not in nlp.pipe_names:
#         ner = nlp.create_pipe('ner')
#         nlp.add_pipe(ner,before='ner')
#     else:
#         ner = nlp.get_pipe('ner')
#     for _, annotations in TRAIN_DATA:
#         for ent in annotations.get('entities'):
#             ner.add_label(ent[2])
#     other_pipes = [pipe for pipe in nlp.pipe_names if pipe != 'ner']
#     with nlp.disable_pipes(*other_pipes):  # only train NER
#         optimizer = nlp.begin_training()
#         for itn in range(n_iter):
#             random.shuffle(TRAIN_DATA)
#             losses = {}
#             for text, annotations in TRAIN_DATA:
#                 nlp.update([text], [annotations], drop=0.5, sgd=optimizer, losses=losses)
# train_entity(output_dir, output_dir, n_iter)

def train_model(model=None, output_dir=None, n_iter=20):
    if 'ner' not in nlp.pipe_names:
        ner = nlp.create_pipe('ner')
        nlp.add_pipe(ner,before='ner')
    else:
        ner = nlp.get_pipe('ner')
    for _, annotations in TRAIN_DATA:
        for ent in annotations.get('entities'):
            ner.add_label(ent[2])
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != 'ner']
    with nlp.disable_pipes(*other_pipes):  # only train NER
        optimizer = nlp.begin_training()
        for itn in range(n_iter):
            random.shuffle(TRAIN_DATA)
            losses = {}
            for text, annotations in TRAIN_DATA:
                nlp.update([text], [annotations], drop=0.5, sgd=optimizer, losses=losses)

    if(len(nlp(u'Test').cats) > 0):
        nlp.disable_pipes('textcat')
    if 'textcat' not in nlp.pipe_names:
        textcat = nlp.create_pipe('textcat')
        nlp.add_pipe(textcat,last=True)
    else:
        textcat = nlp.get_pipe('textcat')
    for cat in np.unique(Y_train):
        if not cat in textcat.labels:
            textcat.add_label(cat)
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != 'textcat']
    with nlp.disable_pipes(*other_pipes):  # only train textcat
        optimizer = nlp.begin_training()
        for i in range(n_iter):
            losses = {}
            # batch up the examples using spaCy's minibatch
            batches = minibatch(train_data, size=compounding(4., 32., 1.001))
            for batch in batches:
                texts, annotations = zip(*batch)
                nlp.update(texts, annotations, sgd=optimizer, drop=0.2, losses=losses)
            #print(losses)            
train_model(output_dir, output_dir, n_iter)

if output_dir is not None:
        output_dir = Path(output_dir)
        if not output_dir.exists():
            output_dir.mkdir()
        nlp.to_disk(output_dir)
        utterances =  '[ '
        intentCounter = 0
        for utterance in data['utterances']:
            counter= 0
            if(intentCounter > 0):
                utterances += ','
            utterances += '{"utterance" : "' + utterance['text'] +'"'
            doc = nlp(utterance['text'])
            utterances += ',"entitiestagged":'+json.dumps(utterance['entities'])
            utterances += ', "pos":{"nouns" : ['
            for token in doc:
                if (token.pos_ == "NOUN" or token.pos_ == "NOUN_NN" or token.pos_ == "NOUN_NNS" and token.is_stop == False):
                    if(counter > 0):
                        utterances += ','
                    utterances += '{"value" : "' + token.text + '"}'
                    counter = counter + 1
            utterances += '], "propernouns" : ['
            counter= 0
            for token in doc:
                if (token.pos_ == "PROPN" or token.pos_ == "PROPN_NNP"and token.is_stop == False):
                    if(counter > 0):
                        utterances += ','
                    utterances += '{"value" : "' + token.text + '"}'
                    counter = counter + 1
            utterances += '], "verbs" : ['
            counter= 0
            for token in doc:
                if (token.pos_ == "VERB" or token.pos_ == "VERB_VBN"):
                    if(counter > 0):
                        utterances += ','
                    utterances += '{"value" : "' + token.text + '"}'
                    counter = counter + 1
            utterances += '], "adjectives" : ['
            counter = 0
            for token in doc:
                if (token.pos_ == "ADJ" or token.pos_ == "ADJ_PDT" and token.is_stop == False ):
                    if(counter > 0):
                        utterances += ','
                    utterances += '{"value" : "' + token.text + '"}'
                    counter = counter + 1
            utterances += ']}, "number" : ['
            counter = 0
            for token in doc:
                if (token.pos_ == "NUM_CD" ):
                    if(counter > 0):
                        utterances += ','
                    utterances += '{"value" : "' + token.text + '"}'
                    counter = counter + 1
            utterances += '],  "symbol" : ['
            counter = 0
            for token in doc:
                if (token.pos_ == "X_XX" or token.pos_ == "SYM_SYM" or token.pos_ == "SYM_$" or token.pos_ == "PUNCT_HYPH"):
                    if(counter > 0):
                        utterances += ','
                    utterances += '{"value" : "' + token.text + '"}'
                    counter = counter + 1
            utterances += '], "entities" : ['
            counter= 0
            for ent in doc.ents:
                if(counter > 0):
                    utterances += ','
                utterances += '{ "entity_type" : "' + ent.label_ + '", "value" : "' + ent.text + '"}' 
                counter = counter + 1
            utterances += '], "posg" : ['
            counter= 0
            for token in doc:
                if(counter > 0):
                    utterances += ','
                utterances += '{"word" : "' + token.text + '","value" : "' + token.pos_ + '"}' 
                counter = counter + 1
            utterances += '], "scores":' + '[' + str(doc.cats).replace("'", '"') + ']}'
            intentCounter = intentCounter + 1 
        utterances +=  ']'
        print(utterances)