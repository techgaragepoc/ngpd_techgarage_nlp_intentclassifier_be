import random
from pathlib import Path
import spacy, sys


# You need to define a mapping from your data's part-of-speech tag names to the
# Universal Part-of-Speech tag set, as spaCy includes an enum of these tags.
# See here for the Universal Tag Set:
# http://universaldependencies.github.io/docs/u/pos/index.html
# You may also specify morphological features for your tags, from the universal
# scheme.
TAG_MAP = {
    'N': {'pos': 'NOUN'},
    'V': {'pos': 'VERB'},
    'J': {'pos': 'ADJ'}
}

#TRAIN_DATA = [
#    ("I like green eggs", {'tags': ['N', 'V', 'J', 'N']}),
#    ("Eat blue ham", {'tags': ['V', 'J', 'N']})
#]

TRAIN_DATA = eval(sys.argv[1])
output_dir = sys.argv[2]
n_iter = 20

def trainpostagger(model=None, output_dir=None, n_iter=25):
    #if model is not None:
    #    nlp = spacy.load(model)  # load existing spaCy model
    #else:
    nlp = spacy.blank('en')  # create blank Language class

    # add the tagger to the pipeline
    # nlp.create_pipe works for built-ins that are registered with spaCy
    if 'tagger' not in nlp.pipe_names:
        tagger = nlp.create_pipe('tagger')
        nlp.add_pipe(tagger, first=True)
    # otherwise, get it, so we can add labels to it
    else:
        tagger = nlp.get_pipe('tagger')
    # Add the tags. This needs to be done before you start training.
    for tag, values in TAG_MAP.items():
        tagger.add_label(tag, values)
    #nlp.add_pipe(tagger)
    # get names of other pipes to disable them during training
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != 'tagger']
    with nlp.disable_pipes(*other_pipes):  # only tagger
        optimizer = nlp.begin_training()
        for i in range(n_iter):
            random.shuffle(TRAIN_DATA)
            losses = {}
            for text, annotations in TRAIN_DATA:
                nlp.update([text], [annotations], sgd=optimizer, losses=losses)
    _tagger = nlp.get_pipe('tagger')
    nlp2 = spacy.load(model)
    nlp2.replace_pipe('tagger', _tagger)
    # save model to output directory
    if output_dir is not None:
        output_dir = Path(output_dir)
        if not output_dir.exists():
            output_dir.mkdir()
        nlp2.to_disk(output_dir)
        print("Training completed successfully")



trainpostagger(output_dir, output_dir, n_iter)

