import spacy 
import json
import sys


model = 'AIMEE_7' #sys.argv[2]
nlp = spacy.load(model)
doc = nlp('tell me your name') #nlp(u'' + sys.argv[1]) 

intent = max(doc.cats, key=doc.cats.get)

counter = 0
data =  '{ "Intent": "' + intent + '", "OhterIntents" : [' + str(doc.cats).replace("'", '"')
data += ' ], "Entities" : ['
counter = 0
for ent in doc.ents:
    if(counter > 0):
        data += ','
    data += '{ "entity_type" : "' + ent.label_ + '", "value" : "' + ent.text + '"}' 
    counter = counter + 1
data += '], "Nouns" : ['
counter = 0
for token in doc:
    data +=  ''
    if (token.pos_ == "PROPN" or token.pos_ == "NOUN" or token.pos_ == "NOUN_N" ):
        if(counter > 0):
            data += ','
        data += '{ "type" : "' + token.pos_ + '", "value" : "' + token.text + '", "lemma" : "' + token.lemma_ + '"}' 
        counter = counter + 1
data += '], "Verbs" : ['
counter= 0
for token in doc:
    if (token.pos_ == "VERB" or token.pos_ == "VERB_VBN"):
        if(counter > 0):
            data += ','
        data += '{ "type" : "' + token.pos_ + '", "value" : "' + token.text + '", "lemma" : "' + token.lemma_ + '"}' 
        counter = counter + 1
data += '], "ADJ" : ['
counter = 0
for token in doc:
    if (token.pos_ == "ADJ" and token.is_stop == False ):
        if(counter > 0):
            data += ','
        data += '{ "type" : "' + token.pos_ + '", "value" : "' + token.text + '", "lemma" : "' + token.lemma_ + '"}' 
        counter = counter + 1
    #print(token.text, token.lemma_, token.pos_, token.tag_, token.dep_,
    #     token.shape_, token.is_alpha, token.is_stop)
# data += ' ], "Parse_Tree" : ['
# counter = 0
# for token in doc:
#     if(counter > 0):
#         data += ','
#     data += '{ "node" : "' + token.text + '", "parent" : "' + token.head.text + '"}' #, "child" : "' + ''.join([str(child) for child in token.children]) + '"},' 
#     counter = counter + 1

# data += '], "POS" : ['
# counter = 0
# for token in doc:
#     if(counter > 0):
#         data += ','
#     data += '{ "word" : "' + token.text + '", "pos" : "' + token.pos_
#     if(token.tag_ != '""'):
#         data += '_' + token.tag_
#     data += '"}'
#     counter = counter + 1
data += ']'
data += '}'
json_Data = json.dumps(data)

print(json_Data)
