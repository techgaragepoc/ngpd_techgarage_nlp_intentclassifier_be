import spacy
# from nltk import Tree
from spacy import displacy
import sys

# model = sys.argv[2]
nlp = spacy.load('en')

doc = nlp(sys.argv[1])
# print(displacy.serve(doc, style='dep'))
print(displacy.render(doc, style='dep'))
#def to_nltk_tree(node):
#    if node.n_lefts + node.n_rights > 0:
#        return Tree(node.orth_, [to_nltk_tree(child) for child in node.children])
#    else:
#        return node.orth_


#[to_nltk_tree(sent.root).pretty_print() for sent in doc.sents]
#for token in doc:
#    print(token.text, token.dep_)
#print(doc.cats)
