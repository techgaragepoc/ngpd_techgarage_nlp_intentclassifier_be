#from spacy.en import English
import spacy
#spacy.load('en')
from spacy.lang.en import English
from spacy.gold import GoldParse
import sys
#
nlp = spacy.load("en")
entity_data = sys.argv[1]
entity_splitdata = entity_data.split("=")
doc = ""
entity_elements = ""
ner = []
#
if len(entity_splitdata) == 2:
    entity_elements = entity_splitdata[1].split(",")
    doc = entity_splitdata[1].replace(',', ' ')

    doc = " ".join(doc.split())
    for temp in entity_elements:
        
        element = temp.split()
        i = 0
        if (len(element) > 1):
            for text in element:
                _str = ""
                if i == 0:
                    _str += "B-" + entity_splitdata[0]
                elif i + 1 == len(element):
                    _str += "L-" + entity_splitdata[0]
                else:
                    _str += "I-" + entity_splitdata[0]
                i = i + 1
                ner.append(_str)
        else :   
            ner.append("U-" + entity_splitdata[0])
    #nlp = English(parser=False) # Avoid loading the parser, for quick load times
    # Run the tokenizer and tagger (but not the entity recognizer)
    print(doc)
    print(entity_splitdata[0])
    print(ner)
    doc = nlp(doc)
    nlp.tagger(doc) 
    nlp.entity.add_label(entity_splitdata[0]) # <-- New in v0.100
    indices = tuple(range(len(doc)))
    words = [w.text for w in doc]
    tags = [w.tag_ for w in doc]
    heads = [0 for _ in doc]
    deps = ['' for _ in doc]
    # Create the GoldParse
    ner = ['U-COMPETENCY', 'U-COMPETENCY', 'U-COMPETENCY']
    annot = GoldParse(doc, entities=ner)
    loss = nlp.entity.update([doc], [annot])
    i = 0
    while loss != 0 and i < 1000:
        loss = nlp.entity.update([doc], [annot])
        i += 1
    print("training successful. Used %d iterations for training" % i)
else:
    print("Invalid Input. Please provide data in correct format (entity_name=value1,value2,value3...)")