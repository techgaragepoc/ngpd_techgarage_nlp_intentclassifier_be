import spacy
from spacy import displacy

def generatePOSTree(userquery):
    nlp = spacy.load('en')
    doc = nlp(userquery)
    return(displacy.render(doc, style='dep'))